<?php

namespace justCMS\NewsBundle\Controller;

use Doctrine\ORM\EntityManager;
use justCMS\DatabaseBundle\Entity\NewsComment;
use justCMS\DatabaseBundle\Form\NewsCommentType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class NewsController extends Controller
{
    public function indexAction(Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()
            ->getManager();

        $query = $em->getRepository('justCMSDatabaseBundle:News')->getApprovedNewsQuery();

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            10
        );

        return $this->render('@justCMSTemplate/NewsBundle/index.twig', [
            'title' => 'Новости',
            'news' => $pagination
        ]);
    }

    public function showAction($id, Request $request)
    {
        $news = $this->getDoctrine()->getRepository('justCMSDatabaseBundle:News')->find($id);

        $commentForm = $this->createForm(
            new NewsCommentType()
        );

        if ($request->getMethod() == 'POST') {
            $commentForm->handleRequest($request);
            if ($commentForm->isValid()) {
                $data = $commentForm->getData();
                $newsComment = new NewsComment();
                $newsComment
                    ->setContent($data['content'])
                    ->setNews($news)
                    ->setUser($this->getUser());
                $this->getDoctrine()->getManager()->persist($newsComment);
                $this->getDoctrine()->getManager()->flush();

                return $this->redirectToRoute(
                    'just_cms_news_show',
                    [
                        "id" => $id
                    ]
                );
            }
        }

        $query = $this->getDoctrine()->getRepository('justCMSDatabaseBundle:NewsComment')->geCommentsByNewsQuery($id);

        $paginator = $this->get('knp_paginator');
        $comments = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            10
        );

        return $this->render(
            '@justCMSTemplate/NewsBundle/show.twig',
            [
                'news' => $news,
                'title' => $news->getTitle(),
                'comments' => $comments,
                'commentForm' => $commentForm->createView()
            ]
        );
    }

    public function listByCatAction($slug, Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()
            ->getManager();

        $query = $em->getRepository('justCMSDatabaseBundle:News')->getApprovedByCatQuery($slug);

        $cat = $em->getRepository('justCMSDatabaseBundle:NewsCategory')->findOneBy(['slug' => $slug]);

        $paginator = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $query,
            $request->query->get('page', 1),
            10
        );

        return $this->render(
            '@justCMSTemplate/NewsBundle/index.twig',
            [
                'title' => $cat->getTitle(),
                'news' => $pagination
            ]
        );
    }
}
