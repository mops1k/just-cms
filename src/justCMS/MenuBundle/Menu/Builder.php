<?php
namespace justCMS\MenuBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\DependencyInjection\ContainerAware;

class Builder extends ContainerAware
{
    public function mainMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav');

        $menu->addChild('News', array('route' => 'just_cms_news'));

        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->container->get('doctrine')->getManager();

        $pages = $em->getRepository('justCMSDatabaseBundle:Page')->findAll();

        if (count($pages) > 0) {
            foreach ($pages as $page) {
                if ($this->isGranted($page->getGroups())) {
                    if (!isset($menu['pagesContainer'])) {
                        $menu->addChild('pagesContainer', array('label' => 'Материалы'))
                            ->setAttribute('dropdown', true)
                            ->setAttribute('icon', 'glyphicon glyphicon-file');
                    }
                    $menu['pagesContainer']->addChild(
                        $page->getTitle(),
                        array(
                            'route' => 'just_cms_page_view',
                            'routeParameters' => [
                                'name' => $page->getSlug()
                            ]
                        )
                    );
                }
            }
        }

        $menu->addChild('Feedback', array('route' => 'just_cms_feedback_write'));

        return $menu;
    }

    public function userMenu(FactoryInterface $factory, array $options)
    {
        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav navbar-nav navbar-right');

        if ($this->container->get('security.authorization_checker')->isGranted(array('ROLE_ADMIN', 'ROLE_USER'))) {
            $username = $this->getUser()->getUsername();

            $menu->addChild('User', array('label' => $username))
                ->setAttribute('dropdown', true)
                ->setAttribute('icon', 'glyphicon glyphicon-user');

            $menu['User']->addChild('Profile', array('route' => 'fos_user_profile_show'))
                ->setAttribute('icon', 'glyphicon glyphicon-edit');
            $menu['User']->addChild('Messages', array('route' => 'fos_message_inbox'))
                ->setAttribute('icon', 'glyphicon glyphicon-edit');
            $menu['User']->addChild('Log Out', array('route' => 'fos_user_security_logout'))
                ->setAttribute('divider_prepend', true)
                ->setAttribute('icon', 'glyphicon glyphicon-log-out');;
            if ($this->container->get('security.authorization_checker')->isGranted('ROLE_MODERATOR')) {
                $menu['User']->addChild('Administration', array('route' => 'sonata_admin_dashboard'))
                    ->setAttribute('icon', 'ico-cabinet')
                    ->setAttribute('divider_prepend', true);
            }
        } else {
            $menu->addChild('User', array('label' => 'Guest'))
                ->setAttribute('dropdown', true)
                ->setAttribute('icon', 'glyphicon glyphicon-user');

            $menu['User']->addChild('Log IN', array('route' => 'fos_user_security_login'))
                ->setAttribute('icon', 'glyphicon glyphicon-log-in');
            $menu['User']->addChild('Register', array('route' => 'fos_user_registration_register'))
                ->setAttribute('icon', 'ico-plus');
        }


        return $menu;
    }

    public function categoryMenu(FactoryInterface $factory, array $options)
    {
        /** @var \Doctrine\ORM\EntityManager $em */
        $em = $this->container->get('doctrine')->getManager();

        $categories = $em->getRepository('justCMSDatabaseBundle:NewsCategory')->findAll();

        $menu = $factory->createItem('root');
        $menu->setChildrenAttribute('class', 'nav nav-list');

        foreach ($categories as $category) {
            $menu->addChild(
                $category->getTitle(),
                [
                    "route" => 'just_cms_news_by_cat',
                    "routeParameters" => [
                        'slug' => $category->getSlug()
                    ]
                ]
            );
        }


        return $menu;
    }

    /**
     * Check granted roles by group
     *
     * @param $groups
     * @return bool
     */
    public function isGranted($groups = [])
    {
        if (count($groups) == 0 OR $groups == null) {
            return true;
        }
        foreach ($groups as $group) {
            $finded = $this->container->get('fos_user.group_manager')->findGroupByName($group->getName());
            if ($this->container->get('security.authorization_checker')->isGranted($finded->getRoles())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get a user from the Security Token Storage.
     *
     * @return mixed
     *
     * @throws \LogicException If SecurityBundle is not available
     *
     * @see TokenInterface::getUser()
     */
    public function getUser()
    {
        if (!$this->container->has('security.token_storage')) {
            throw new \LogicException('The SecurityBundle is not registered in your application.');
        }

        if (null === $token = $this->container->get('security.token_storage')->getToken()) {
            return;
        }

        if (!is_object($user = $token->getUser())) {
            // e.g. anonymous authentication
            return;
        }

        return $user;
    }
}