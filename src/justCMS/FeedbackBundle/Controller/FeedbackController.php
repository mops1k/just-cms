<?php
/**
 * Created by PhpStorm.
 * User: kvintiljanov
 * Date: 02.06.2015
 * Time: 10:17
 */

namespace justCMS\FeedbackBundle\Controller;


use justCMS\DatabaseBundle\Entity\Feedback;
use justCMS\DatabaseBundle\Form\FeedbackType;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;

class FeedbackController extends Controller
{
    public function indexAction(Request $request)
    {
        $entity = new Feedback();
        $form = $this->createForm(new FeedbackType(), $entity);

        if ($request->getMethod() === 'POST') {
            if ($request->getSession()->get('feedback_send') != '1') {
                $form->handleRequest($request);
                if ($form->isValid()) {
                    $data = $form->getData();

                    $message = \Swift_Message::newInstance()
                        ->setSubject('Feedback from user: '.$data->getName())
                        ->setFrom($data->getEmail())
                        ->setTo('bednyj.mops@gmail.com')
                        ->setBody(
                            $this->renderView(
                                '@justCMSTemplate/Emails/feedback.twig',
                                array('content' => $data->getFeedback())
                            ),
                            'text/html'
                        );

                    $this->get('mailer')->send($message);

                    $em = $this->getDoctrine()->getManager();

                    $em->persist($data);
                    $em->flush();
                    $request->getSession()->set('feedback_send', '1');
                }
            } else {
                throw new Exception('Вы уже отправляли сообщение.');
            }
        }

        return $this->render(
            '@justCMSTemplate/FeedbackBundle/feedback.twig',
            [
                'title' => 'feedback',
                'feedback_form' => $form->createView()
            ]
        );
    }
}