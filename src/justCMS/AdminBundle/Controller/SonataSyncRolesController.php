<?php

namespace justCMS\AdminBundle\Controller;

use justCMS\DatabaseBundle\Entity\Role;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;

class SonataSyncRolesController extends Controller
{
    public function rolesAction(Request $request)
    {
        if (!$this->isGranted('ROLE_SUPER_ADMIN')) {
            throw new AccessDeniedHttpException();
        }

        // Если нет сонаты, выходим, генерировать нечего
        if (!$this->container->has('sonata.admin.pool')) {
            throw new \LogicException('The SonataAdminBundle is not registered in your application.');
        }

        // ini_set('memory_limit', '-1');
        $translator = $this->get('translator');

        $roles = null;

        // получаем список сервисов
        $services = $this->container->get('sonata.admin.pool')->getAdminServiceIds();

        $em = $this
            ->getDoctrine()
            ->getManagerForClass("justCMS\\DatabaseBundle\\Entity\\Role");

        $_emr = $em->getRepository('justCMSDatabaseBundle:Role');

        // Генерируем список новых ролей Sonata Admin Bundle
        $i = 0;
        foreach ($services as $service) {
            $admin = $this->container->get('sonata.admin.pool')->getAdminByAdminCode($service);
            $serviceTrans = strtoupper(str_replace('.', '_', $service));
            $parentId = $i;
            $roles[$i] = [
                'name' => 'ROLE_'.$serviceTrans,
                'title' => $admin->trans($admin->getLabel()).' ПОЛНЫЙ ДОСТУП',
                'description' => 'Auto generated Sonata GROUP: '.$service,
                'type' => 'parent',
                'parent' => null,
            ];
            foreach ($admin->getSecurityInformation() as $value) {
                foreach ($value as $val) {
                    ++$i;
                    $roles[$i] = [
                        'name' => 'ROLE_'.$serviceTrans.'_'.$val,
                        'title' => $admin->trans($this->container->get($service)->getLabel()).' '.$translator->trans(
                                $val
                            ),
                        'description' => 'Auto generated Sonata '.$val.' role: '.$service,
                        'type' => 'child',
                        'parent' => $parentId,
                    ];
                }
            }
            ++$i;
        }

        // Получаем все сгенерированные ранее роли
        $generatedRoles = $_emr
            ->findBy(
                [
                    "generated" => true,
                ]
            );

        // Получаем списки добавления и удаления
        $addList = $this->diff($roles, $generatedRoles);
        $deleteList = $this->diff($generatedRoles, $roles);

        // Удаляем роли для удаления и, если есть привязанные
        // к пользователям роли, отвязываем
        foreach ($deleteList as $role) {
            $em->remove($role);
        }
        $em->flush();

        // Добавляем новые роли
        $parent = [];
        foreach ($addList as $key => $role) {
            // создаем генерируемую роль
            $object = new Role();
            $object
                ->setName($role['name'])
                ->setTitle($role['title'])
                ->setDescription($role['description'])
                ->setGenerated(true);
            $em->persist($object);

            // если это роль родитель
            if ($role['type'] == 'parent') {
                $parent[$key] = $object;
            }
            if ($role['type'] == 'child') {
                // если зависимая роль и родитель не определен,
                // то определяем его
                if (!isset($parent[$role['parent']])) {
                    $parent[$role['parent']] = $_emr
                        ->findOneBy(
                            [
                                'name' => $roles[$role['parent']]['name'],
                            ]
                        );
                }
                // добавляем ребенка к родителю
                $parent[$role['parent']]->addChild($object);
                $em->persist($parent[$role['parent']]);
            }
        }

        // выполняем запись всех операций в базу
        $em->flush();

        $session = $this->get('session');
        $session->getFlashBag()->add('sonata_flash_success', 'Роли синхронизированы с Sonata в БД');
        $url = $request->headers->get('referer') ?: '/admin/dashboard';

        return new RedirectResponse($url);
    }

    /**
     * Метод для сравнения новых и старых ролей.
     *
     * @param array $target Массив ролей (массив строк или объектов) из которого вытаскиваем различия
     * @param array $search Массив ролей (массив строк или объектов) с которым сравниваем
     *
     * @return array
     */
    private function diff($target, $search)
    {
        $ret = [];
        foreach ($target as $key => $value) {
            $diff = true;
            foreach ($search as $val) {
                if (is_object($value)) {
                    if ($value->getName() == $val['name']) {
                        $diff = false;
                        break;
                    }
                } elseif (is_object($val)) {
                    if ($value['name'] == $val->getName()) {
                        $diff = false;
                        break;
                    }
                }
            }
            if ($diff) {
                $ret[$key] = $value;
            }
        }

        return $ret;
    }
}
