<?php

namespace justCMS\AdminBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class justCMSAdminBundle extends Bundle
{
    public function getParent()
    {
        return 'SonataAdminBundle';
    }
}
