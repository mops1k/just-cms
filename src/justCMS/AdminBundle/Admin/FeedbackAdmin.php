<?php
namespace justCMS\AdminBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;

class FeedbackAdmin extends Admin
{
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create')
            ->remove('edit')
            ->remove('delete');
    }

    protected function configureListFields(ListMapper $list)
    {
        $list
            ->add('name')
            ->add('email')
            ->add('feedback')
            ->add(
                '_action',
                'actions',
                [
                    'actions' => [
                        'show' => []
                    ]
                ]
            );
    }

    protected function configureShowFields(ShowMapper $filter)
    {
        $filter
            ->add('name')
            ->add('email')
            ->add('feedback');
    }

    protected function configureDatagridFilters(DatagridMapper $filter)
    {
        $filter
            ->add('name')
            ->add('email');
    }


}