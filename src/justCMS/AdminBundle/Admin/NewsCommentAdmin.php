<?php
namespace justCMS\AdminBundle\Admin;

use justCMS\DatabaseBundle\Entity\News;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

class NewsCommentAdmin extends Admin
{
    protected $datagridValues = array(
        '_page' => 1,            // display the first page (default = 1)
        '_sort_order' => 'DESC', // reverse order (default = 'ASC')
        '_sort_by' => 'date'  // name of the ordered field
    );

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection
            ->remove('create');

    }

    /**
     * {@inheritdoc}
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('date')
            ->add('content')
            ->add('user')
            ->add('news')
            ->add(
                '_action',
                'actions',
                array(
                    'actions' => array(
                        'edit' => array(),
                        'delete' => array(),
                    )
                )
            );
    }

    /**
     * {@inheritdoc}
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('news')
            ->add('user');
    }

    protected function configureFormFields(FormMapper $form)
    {
        $form
            ->add(
                'date',
                null,
                [
                    'widget' => 'single_text',
                    'read_only' => true,
                    'disabled' => true,
                    'required' => false
                ]
            )
            ->add(
                'news',
                null,
                [
                    'read_only' => true,
                    'disabled' => true,
                    'required' => false
                ]
            )
            ->add(
                'user',
                null,
                [
                    'read_only' => true,
                    'disabled' => true,
                    'required' => false
                ]
            )
            ->add(
                'content',
                'textarea',
                [
                    'attr' => [
                        'class' => 'wysibb'
                    ]
                ]
            );
    }
}