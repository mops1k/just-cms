<?php

namespace justCMS\AdminBundle\Admin;

use Doctrine\ORM\QueryBuilder;
use Knp\Menu\ItemInterface as MenuItemInterface;
use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Admin\AdminInterface;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Show\ShowMapper;

class RoleAdmin extends Admin
{
    public $supportsPreviewMode = true;

    protected $datagridValues = array(
        'generated' => array(
            'type' => 1,
            'value' => 2
        ),
    );

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add(
                'name',
                'text',
                array(
                    "by_reference" => false,
                    'help' => 'Имя роли латиницей (пример: ROLE_GUEST)'
                )
            )
            ->add(
                'title',
                'text',
                [
                    'help' => 'Человекочитаемое название роли (пример: Гость)'
                ]
            )
            ->add(
                'description',
                'textarea',
                [
                    'label' => 'Описание',
                    'help' => 'Более подробное описание роли',
                    'attr' => [
                        'class' => 'ckeditor'
                    ]
                ]
            )
            ->add(
                'children'
            );
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name')
            ->add('title')
            ->add(
                'children.title',
                null,
                [
                    'label' => 'Содержит роль'
                ]
            )
            ->add(
                'generated',
                'doctrine_orm_callback',
                array(
                    'label' => 'Происхождение роли',
                    'field_type' => 'choice',
                    'field_options' => array(
                        'required' => false,
                        'choices' => [
                            '' => 'Все',
                            1 => 'Сгенерированные',
                            2 => 'Добавленные вручную',
                        ],
                    ),
                    'callback' => function ($queryBuilder, $alias, $field, $value) {

                        if (!$value['value']) {
                            return false;
                        }
                        /* @var $queryBuilder QueryBuilder */

                        if ($value['value'] == 1) {
                            $queryBuilder
                                ->select($alias)
                                ->andWhere($alias.'.generated = true');
                        } elseif ($value['value'] == 2) {
                            $queryBuilder->select($alias)
                                ->andWhere($alias.'.generated = false');
                        }

                        return true;
                    },
                )
            )
            ->add(
                'group',
                'doctrine_orm_callback',
                array(
                    'label' => 'Тип роли',
                    'field_type' => 'choice',
                    'field_options' => array(
                        'required' => false,
                        'choices' => [
                            '' => 'Все',
                            1 => 'Группы',
                            2 => 'Не группы',
                        ],
                    ),
                    'callback' => function ($queryBuilder, $alias, $field, $value) {

                        if (!$value['value']) {
                            return false;
                        }
                        /* @var $queryBuilder QueryBuilder */

                        if ($value['value'] == 1) {
                            $queryBuilder
                                ->select($alias.',c')
                                ->leftJoin(sprintf('%s.children', $alias), 'c')
                                ->andWhere('c.id is not null');
                        } elseif ($value['value'] == 2) {
                            $queryBuilder
                                ->select($alias.',c')
                                ->leftJoin(sprintf('%s.children', $alias), 'c')
                                ->andWhere('c.id is null');
                        }

                        return true;
                    },
                )
            );
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name')
            ->addIdentifier('title')
            ->addIdentifier('children')
            ->add(
                '_action',
                'actions',
                array(
                    'actions' => array(
                        'show' => array(),
                        'edit' => array(),
                        'delete' => array(),
                    ),
                )
            );
    }

    protected function configureShowFields(ShowMapper $filter)
    {
        $filter
            ->add('name')
            ->add('title')
            ->add('description')
            ->add('children');
    }

    protected function configureTabMenu(MenuItemInterface $menu, $action, AdminInterface $childAdmin = null)
    {
        $menu->addChild(
            'sync',
            [
                'label' => 'Синхронизировать роли админпанели',
                'route' => 'just_cms_role_sync'
            ]
        );
    }

}
