<?php
namespace justCMS\UserBundle\Twig;


class Md5HashExtension extends \Twig_Extension
{
    public function getFilters()
    {
        return [
            new \Twig_SimpleFilter('md5', 'md5')
        ];
    }

    public function getFunctions()
    {
        return [
            new \Twig_SimpleFunction('md5', 'md5')
        ];
    }


    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'md5_hash';
    }

}

/*
services.yml:
twig.md5:
    class: justCMS\UserBundle\Twig\Md5HashExtension
    tags :
        - { name: twig.extension }
 */