<?php


namespace justCMS\UserBundle\Model;


use Recaptcher\Exception\Exception;
use Recaptcher\Exception\InvalidRecaptchaException;

class Recaptcha extends \Recaptcher\Recaptcha
{
    public function getWidgetHtml(array $options = array())
    {
        if (!empty($options)) {
            $optionsHtml = '<script type="text/javascript">var RecaptchaOptions = '.json_encode($options).';</script>';
        } else {
            $optionsHtml = '';
        }

        // 'lang' option is buggy � we need to pass it as paremeter in the URL
        $lang = isset($options['lang']) ? $options['lang'] : null;

        return "<script src='https://www.google.com/recaptcha/api.js'></script>".$this->getChallenge();
    }

    /**
     * @see https://groups.google.com/d/msg/recaptcha/o-YdYJlnRVM/RciK7IEPmRkJ
     * @param string $lang
     * @return string
     */
    protected function getChallenge($lang = null)
    {
        return '<div class="g-recaptcha" data-sitekey="'.$this->publicKey.'"></div>';
    }

    /**
     * Sends HTTP POST to server to verify user's input
     *
     * @param string $remoteIp
     * @param string $challengeValue
     * @param string $responseValue
     * @param array $params an array of extra parameters to POST to the server
     * @throws \Recaptcher\Exception\Exception
     * @throws \Recaptcher\Exception\InvalidRecaptchaException
     * @return bool
     */
    public function checkAnswer($remoteIp, $challengeValue, $responseValue, array $params = array())
    {
        if (!$remoteIp) {
            throw new Exception('You must pass the remote ip to reCAPTCHA');
        }

        $response = $this->httpPost(
            "https://www.google.com",
            '/recaptcha/api/siteverify',
            [
                'secret' => $this->privateKey,
                'remoteip' => $remoteIp,
                'response' => $responseValue
            ]
        );

        $json = json_decode($response, true);

        return $json['success'];
    }

    /**
     * @return string
     */
    public function getResponseField()
    {
        return 'g-recaptcha-response';
    }

    /**
     * Sends an HTTP POST and returns response
     *
     * @param string $host
     * @param string $path
     * @param array $data
     * @param int $port
     * @throws \Recaptcher\Exception\Exception
     * @return array
     */
    protected function httpPost($host, $path, $data, $port = 80)
    {
        if ($curl = curl_init()) {
            $i = 1;
            $data_string = null;
            foreach ($data as $key => $value) {
                $data_string .= $key."=".$value;
                if ($i !== count($data)) {
                    $data_string .= "&";
                }
                ++$i;
            }

            curl_setopt($curl, CURLOPT_URL, $host.$path);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);
            $out = curl_exec($curl);
            curl_close($curl);
        }

        return $out;
    }
}