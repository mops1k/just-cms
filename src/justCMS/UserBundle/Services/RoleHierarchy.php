<?php
namespace justCMS\UserBundle\Services;

use justCMS\DatabaseBundle\Entity\Role;
use Doctrine\Common\Cache\CacheProvider;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;

class RoleHierarchy extends \Symfony\Component\Security\Core\Role\RoleHierarchy
{
    /** @var EntityManager em */
    private $em;
    private $hierarchy;

    /**
     * @param array $hierarchy
     */
    public function __construct(array $hierarchy, ManagerRegistry $em, CacheProvider $cache = null)
    {
        $this->em = $em->getManagerForClass("justCMSDatabaseBundle:Role");
        $this->hierarchy = $hierarchy;

        //Пытаемся достать дерево из кэша, если таковой имеется
        $tree = $cache ? $cache->fetch('rolesTree') : null;

        if ($tree) {
            $tree = unserialize($tree); //Нашли в кэше - оживляем
        } else {
            $tree = $this->buildRolesTree();    //Нет в кэше - собираем
            $cache && $cache->save('rolesTree', serialize($tree), 60);
        }

        parent::__construct($tree);
    }


    /**
     * Here we build an array with roles. It looks like a two-levelled tree - just
     * like original Symfony roles are stored in security.yml
     * @return array
     */
    private function buildRolesTree()
    {
        if (isset(self::$tree)) {
            return self::$tree;
        }

        $hierarchy = $this->hierarchy;
        $qb = $this->em->createQueryBuilder();
        $qb->select('r,p,c')
            ->from('justCMSDatabaseBundle:Role', 'r')
            ->leftJoin('r.parents', 'p')
            ->leftJoin('r.children', 'c');
        $query = $qb->getQuery();
        $query->useResultCache(true, 600);
        $query->useQueryCache(true);

        $roles = $query->getResult(AbstractQuery::HYDRATE_ARRAY);
        foreach ($roles as $role) {
            /** @var $role \justCMS\DatabaseBundle\Entity\Role */
            if (count($role['parents'])) {
                foreach ($role['parents'] as $parent) {
                    $hierarchy[$parent['name']][] = $role['name'];
                }
            } else {
                if (!isset($hierarchy[$role['name']])) {
                    $hierarchy[$role['name']] = array();
                }
            }
        }

        return $hierarchy;
    }

    public function getRoles()
    {
        return $this->buildRolesTree();
    }
}