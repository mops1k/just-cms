<?php

namespace justCMS\UserBundle\DependencyInjection\Compiler;


use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;

class OverrideCompilerPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        //Заместить старую рекапчу новой
        $definition = $container->getDefinition('recaptcha');
        $definition->setClass('justCMS\UserBundle\Model\Recaptcha');
    }
}