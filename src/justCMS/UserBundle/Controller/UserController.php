<?php
namespace justCMS\UserBundle\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\Controller;

abstract class UserController extends Controller
{
    /**
     * Check granted roles by group
     *
     * @param array|string $groups
     * @return bool
     */
    public function isGroupGranted($groups = [])
    {
        if (count($groups) == 0 OR $groups == null) {
            return true;
        }
        if (is_array($groups)) {
            foreach ($groups as $group) {
                $finded = $this->container->get('fos_user.group_manager')->findGroupByName($group->getName());
                if ($this->container->get('security.authorization_checker')->isGranted($finded->getRoles())) {
                    return true;
                }
            }
        } else {
            $finded = $this->container->get('fos_user.group_manager')->findGroupByName($groups);
            if ($this->container->get('security.authorization_checker')->isGranted($finded->getRoles())) {
                return true;
            }
        }
        return false;
    }
}