<?php
namespace justCMS\CoreBundle\Events;

use Swift_Transport_EsmtpTransport;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\HttpKernelInterface;
use Symfony\Component\HttpKernel\KernelEvents;

class MailerConfigEvent implements EventSubscriberInterface
{

    /**
     * @var Swift_Transport_EsmtpTransport
     */
    private $transport;

    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $containerInterface)
    {
        $this->transport = $containerInterface->get('mailer')->getTransport();
        $this->container = $containerInterface;
    }

    public function onKernelRequest(GetResponseEvent $event)
    {
        $settings = $this->container->get('just_cms_settings');

        $ext = $this->transport->getExtensionHandlers();
        /** @var \Swift_Transport_Esmtp_AuthHandler $auth_handler */
        $auth_handler = $ext[0];
        $auth_handler->setUserName($settings->getOneByName('smtp_user')->getValue());
        $auth_handler->setPassword($settings->getOneByName('smtp_password')->getValue());

        $this->transport->setEncryption($settings->getOneByName('smtp_encryption')->getValue());
        $this->transport->setHost($settings->getOneByName('smtp_host')->getValue());
        $this->transport->setPort($settings->getOneByName('smtp_port')->getValue());
    }

    public static function getSubscribedEvents()
    {
        return array(
            KernelEvents::REQUEST => 'onKernelRequest',
        );
    }
}