<?php

namespace justCMS\MessageBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class justCMSMessageBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSMessageBundle';
    }
}
