<?php
/**
 * Created by PhpStorm.
 * User: kvintiljanov
 * Date: 02.06.2015
 * Time: 16:13
 */

namespace justCMS\MessageBundle\FormType;


use Symfony\Component\Form\FormBuilderInterface;

class NewThreadMultipleMessageFormType extends \FOS\MessageBundle\FormType\NewThreadMultipleMessageFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        // ToDo: remove self username from multipple select
        $builder
            ->remove('recipients')
            ->add(
                'recipients',
                'entity',
                [
                    'class' => 'justCMSDatabaseBundle:User',
                    'multiple' => true,
                    'attr' => [
                        'class' => 'select2'
                    ]
                ]
            )
            ->remove('body')
            ->add(
                'body',
                'textarea',
                [
                    'attr' => [
                        'rows' => 4,
                        'class' => 'wysibb'
                    ]
                ]
            );
    }

}