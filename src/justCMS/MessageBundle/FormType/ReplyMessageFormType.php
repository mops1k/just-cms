<?php
namespace justCMS\MessageBundle\FormType;

use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/**
 * Form type for a reply
 *
 * @author Thibault Duplessis <thibault.duplessis@gmail.com>
 */
class ReplyMessageFormType extends \FOS\MessageBundle\FormType\ReplyMessageFormType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder
            ->remove('body')
            ->add(
                'body',
                'textarea',
                [
                    'attr' => [
                        'rows' => 4,
                        'class' => 'wysibb'
                    ]
                ]
            );
    }

    public function getName()
    {
        return 'fos_message_reply_message';
    }
}
