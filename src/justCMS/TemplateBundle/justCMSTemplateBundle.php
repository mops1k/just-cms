<?php

namespace justCMS\TemplateBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class justCMSTemplateBundle extends Bundle
{
    public function getParent()
    {
        return 'TwigBundle';
    }
}
