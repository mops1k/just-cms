<?php

namespace justCMS\TemplateBundle\TwigExtensions\Extension;

use justCMS\TemplateBundle\TwigExtensions\Executive\Settings as Executive;


class Settings extends \Twig_Extension
{

    protected $settings;

    public function __construct(Executive $settings)
    {
        $this->settings = $settings;
    }

    public function getGlobals()
    {
        return array(
            'settings' => $this->settings
        );
    }

    public function getName()
    {
        return 'settings';
    }
}