<?php
/**
 * Created by PhpStorm.
 * User: kvintiljanov
 * Date: 19.05.2015
 * Time: 13:09
 */

namespace justCMS\TemplateBundle\TwigExtensions\Executive;

use Doctrine\ORM\EntityManager;

class Settings
{
    /** @var EntityManager $em */
    public $em;

    function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    function getOneByName($name)
    {
        $setting = $this->em->getRepository('justCMSDatabaseBundle:Settings')->findOneBy(
            [
                'name' => $name
            ]
        );

        return $setting;
    }

    function getAll()
    {
        return $this->em
            ->getRepository('justCMSDatabaseBundle:Settings')
            ->findAll();
    }
}