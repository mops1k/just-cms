<?php

namespace justCMS\DatabaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * NewsComment
 *
 * @ORM\Table(name="just_news_comment")
 * @ORM\Entity(repositoryClass="justCMS\DatabaseBundle\Entity\NewsCommentRepository")
 */
class NewsComment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;


    /**
     * Initialize entity default values
     */
    function __construct()
    {
        $this->date = new \DateTime();
    }

    /**
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * @param \DateTime $date
     *
     * @return NewsComment
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * @ORM\ManyToOne(targetEntity="News", inversedBy="comments")
     */
    private $news;

    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    private $user;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     * @return NewsComment
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set news
     *
     * @param \justCMS\DatabaseBundle\Entity\News $news
     * @return NewsComment
     */
    public function setNews(\justCMS\DatabaseBundle\Entity\News $news = null)
    {
        $this->news = $news;

        return $this;
    }

    /**
     * Get news
     *
     * @return \justCMS\DatabaseBundle\Entity\News
     */
    public function getNews()
    {
        return $this->news;
    }

    /**
     * Set user
     *
     * @param \justCMS\DatabaseBundle\Entity\User $user
     * @return NewsComment
     */
    public function setUser(\justCMS\DatabaseBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \justCMS\DatabaseBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }
}
