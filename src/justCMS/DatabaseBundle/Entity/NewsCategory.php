<?php
/**
 * Created by PhpStorm.
 * User: kvintiljanov
 * Date: 14.04.2015
 * Time: 13:46
 */

namespace justCMS\DatabaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Knp\Menu\NodeInterface;

/**
 * @ORM\Table(name="just_news_category")
 * @ORM\Entity()
 */
class NewsCategory
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\Column(name="title", type="string", length=64)
     */
    private $title;

    /**
     * @ORM\OneToMany(targetEntity="News", mappedBy="category")
     */
    private $news;



    /**
     * @ORM\Column(name="slug", type="string", length=128)
     */
    private $slug;

    public function getId()
    {
        return $this->id;
    }

    public function getSlug()
    {
        return $this->slug;
    }

    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getTitle()
    {
        return $this->title;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->news = new \Doctrine\Common\Collections\ArrayCollection();
    }



    /**
     * Set slug
     *
     * @param string $slug
     * @return NewsCategory
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;

        return $this;
    }

    /**
     * Add news
     *
     * @param \justCMS\DatabaseBundle\Entity\News $news
     * @return NewsCategory
     */
    public function addNews(\justCMS\DatabaseBundle\Entity\News $news)
    {
        $this->news[] = $news;

        return $this;
    }

    /**
     * Remove news
     *
     * @param \justCMS\DatabaseBundle\Entity\News $news
     */
    public function removeNews(\justCMS\DatabaseBundle\Entity\News $news)
    {
        $this->news->removeElement($news);
    }

    /**
     * Get news
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getNews()
    {
        return $this->news;
    }

    function __toString()
    {
        return $this->title;
    }
}
