<?php

namespace justCMS\DatabaseBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * News
 *
 * @ORM\Table(name="just_news")
 * @ORM\Entity(repositoryClass="NewsRepository")
 */
class News
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=128)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="less_content", type="text")
     */
    private $lessContent;

    /**
     * @var string
     *
     * @ORM\Column(name="full_content", type="text")
     */
    private $fullContent;

    /**
     * @var int
     *
     * @ORM\Column(name="published", type="boolean")
     */
    private $published;

    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="news")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="NewsCategory", inversedBy="news")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    /**
     * @ORM\OneToMany(targetEntity="NewsComment", mappedBy="news")
     */
    private $comments;

    function __construct()
    {
        $this->date = new \DateTime();
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     * @return News
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return News
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set lessContent
     *
     * @param string $lessContent
     * @return News
     */
    public function setLessContent($lessContent)
    {
        $this->lessContent = $lessContent;

        return $this;
    }

    /**
     * Get lessContent
     *
     * @return string
     */
    public function getLessContent()
    {
        return $this->lessContent;
    }

    /**
     * Set fullContent
     *
     * @param string $fullContent
     * @return News
     */
    public function setFullContent($fullContent)
    {
        $this->fullContent = $fullContent;

        return $this;
    }

    /**
     * Get fullContent
     *
     * @return string
     */
    public function getFullContent()
    {
        return $this->fullContent;
    }

    /**
     * Set published
     *
     * @param boolean $published
     * @return News
     */
    public function setPublished($published)
    {
        $this->published = $published;

        return $this;
    }

    /**
     * Get published
     *
     * @return boolean
     */
    public function getPublished()
    {
        return $this->published;
    }

    /**
     * Set user
     *
     * @param \justCMS\DatabaseBundle\Entity\User $user
     * @return News
     */
    public function setUser(\justCMS\DatabaseBundle\Entity\User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \justCMS\DatabaseBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set category
     *
     * @param \justCMS\DatabaseBundle\Entity\NewsCategory $category
     * @return News
     */
    public function setCategory(\justCMS\DatabaseBundle\Entity\NewsCategory $category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return \justCMS\DatabaseBundle\Entity\NewsCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Add comments
     *
     * @param \justCMS\DatabaseBundle\Entity\NewsComment $comments
     * @return News
     */
    public function addComment(\justCMS\DatabaseBundle\Entity\NewsComment $comments)
    {
        $this->comments[] = $comments;

        return $this;
    }

    /**
     * Remove comments
     *
     * @param \justCMS\DatabaseBundle\Entity\NewsComment $comments
     */
    public function removeComment(\justCMS\DatabaseBundle\Entity\NewsComment $comments)
    {
        $this->comments->removeElement($comments);
    }

    /**
     * Get comments
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    function __toString()
    {
        return $this->getId().': '.$this->getTitle();
    }
}
