<?php
namespace justCMS\DatabaseBundle\Entity;

use Doctrine\ORM\EntityRepository;

class NewsRepository extends EntityRepository
{

    /**
     * Подготавливаем запрос на опубликованные новости
     *
     * @return \Doctrine\ORM\Query
     */
    public function getApprovedNewsQuery()
    {
        $qb = $this->_em->createQueryBuilder();

        $qb->select('n,u,c,m')
            ->from('justCMSDatabaseBundle:News', 'n')
            ->leftJoin('n.user', 'u')
            ->leftJoin('n.category', 'c')
            ->leftJoin('n.comments', 'm')
            ->where('n.published = true')
            ->orderBy('n.date', 'DESC');

        return $qb->getQuery();
    }

    /**
     * Подготавливаем запрос на опубликованные новости по категории
     *
     * @param $cat
     * @return \Doctrine\ORM\Query
     */
    public function getApprovedByCatQuery($cat)
    {
        $qb = $this->_em->createQueryBuilder();

        $qb->select('n,u,c')
            ->from('justCMSDatabaseBundle:News', 'n')
            ->leftJoin('n.user', 'u')
            ->leftJoin('n.category', 'c')
            ->where('n.published = true')
            ->andWhere('c.slug = :slug')
            ->orderBy('n.date', 'DESC')
            ->setParameter(':slug', $cat);

        return $qb->getQuery();
    }
}