<?php
/**
 * Created by PhpStorm.
 * User: kvintiljanov
 * Date: 02.06.2015
 * Time: 10:22
 */

namespace justCMS\DatabaseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class FeedbackType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'name',
                'text',
                [
                    'attr' => [
                        'placeholder' => 'Enter your name'
                    ]
                ]
            )
            ->add(
                'email',
                'email',
                [
                    'attr' => [
                        'placeholder' => 'Enter your email'
                    ]
                ]
            )
            ->add(
                'feedback',
                'textarea',
                [
                    'label' => 'Вопрос или отзыв',
                    'attr' => [
                        'rows' => 7,
                        'placeholder' => 'Enter your feedback'
                    ]
                ]
            )
            ->add('send', 'submit');
    }

    /**
     * Returns the name of this type.
     *
     * @return string The name of this type
     */
    public function getName()
    {
        return 'feedback';
    }

}