<?php

namespace justCMS\DatabaseBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class NewsCommentType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'content',
                'textarea',
                [
                    "label" => 'Ваш комментарий',
                    'attr' => [
                        'rows' => 3
                    ]
                ]
            );
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'justcms_newscomment';
    }
}
