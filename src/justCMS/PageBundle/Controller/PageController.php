<?php

namespace justCMS\PageBundle\Controller;

use Doctrine\ORM\EntityManager;
use justCMS\UserBundle\Controller\UserController;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class PageController extends UserController
{
    public function indexAction($name)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $page = $em->getRepository('justCMSDatabaseBundle:Page')->findOneBy(
            [
                'slug' => $name
            ]
        );

        if ($page === null) {
            throw new NotFoundHttpException();
        }

        if (!$this->isGroupGranted($page->getGroups())) {
            throw new AccessDeniedHttpException();
        }

        return $this->render(
            '@justCMSTemplate/PageBundle/index.twig',
            [
                'title' => $page->getTitle(),
                'content' => $page->getContent()
            ]
        );
    }
}