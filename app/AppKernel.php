<?php

use Symfony\Component\HttpKernel\Kernel;
use Symfony\Component\Config\Loader\LoaderInterface;

class AppKernel extends Kernel
{
    private $uname ;

    public function __construct($environment, $debug)
    {
        $uname = php_uname();
        $list = explode(" ",$uname);
        $this->uname = $list[1];
        parent::__construct($environment, $debug);
    }


    public function registerBundles()
    {
        $bundles = array(
            new Symfony\Bundle\FrameworkBundle\FrameworkBundle(),
            new Symfony\Bundle\SecurityBundle\SecurityBundle(),
            new Symfony\Bundle\TwigBundle\TwigBundle(),
            new Symfony\Bundle\MonologBundle\MonologBundle(),
            new Symfony\Bundle\SwiftmailerBundle\SwiftmailerBundle(),
            new Symfony\Bundle\AsseticBundle\AsseticBundle(),
            new Doctrine\Bundle\DoctrineBundle\DoctrineBundle(),
            new Sensio\Bundle\FrameworkExtraBundle\SensioFrameworkExtraBundle(),

            // FOS
            new FOS\UserBundle\FOSUserBundle(),
            new FOS\MessageBundle\FOSMessageBundle(),
            // BBCode
            new FM\BbcodeBundle\FMBbcodeBundle(),

            // KNP
            new Knp\Bundle\MenuBundle\KnpMenuBundle(),
            new Knp\Bundle\PaginatorBundle\KnpPaginatorBundle(),

            // Add your dependencies
            new Sonata\CoreBundle\SonataCoreBundle(),
            new Sonata\BlockBundle\SonataBlockBundle(),
            new Sonata\DoctrineORMAdminBundle\SonataDoctrineORMAdminBundle(),
            // Then add SonataAdminBundle
            new Sonata\AdminBundle\SonataAdminBundle(),

            // justCMS Bundles
            new justCMS\CoreBundle\justCMSCoreBundle(),
            new justCMS\NewsBundle\justCMSNewsBundle(),
            new justCMS\TemplateBundle\justCMSTemplateBundle(),
            new justCMS\DatabaseBundle\justCMSDatabaseBundle(),
            new justCMS\UserBundle\justCMSUserBundle(),
            new justCMS\MenuBundle\justCMSMenuBundle(),
            new justCMS\AdminBundle\justCMSAdminBundle(),
            new justCMS\PageBundle\justCMSPageBundle(),
            new justCMS\FeedbackBundle\justCMSFeedbackBundle(),
            new justCMS\MessageBundle\justCMSMessageBundle(),
            new Dmishh\Bundle\RecaptchaBundle\RecaptchaBundle()
        );

        if (in_array($this->getEnvironment(), ['dev', 'test'])) {
            $bundles[] = new Symfony\Bundle\DebugBundle\DebugBundle();
            $bundles[] = new Symfony\Bundle\WebProfilerBundle\WebProfilerBundle();
            $bundles[] = new Sensio\Bundle\DistributionBundle\SensioDistributionBundle();
            $bundles[] = new Sensio\Bundle\GeneratorBundle\SensioGeneratorBundle();
        }

        return $bundles;
    }

    public function getCacheDir()
    {
        $cache = parent::getCacheDir();
        if($this->uname == 'devel' && in_array($this->environment, array('dev', 'test'))) {
            $cache = '/tmp/cache/'.$this->environment;
        }
        return $cache;
    }

    public function getLogDir()
    {
        $log = parent::getLogDir();
        if($this->uname == 'devel' && in_array($this->environment, array('dev', 'test'))) {
            $log = '/tmp/log';
        }
        return $log;
    }

    public function registerContainerConfiguration(LoaderInterface $loader)
    {
        $loader->load($this->getRootDir().'/config/config_'.$this->getEnvironment().'.yml');
    }
}
